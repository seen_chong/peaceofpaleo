<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'paleo_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'H@{T!kPP%3Pt}^/cB-ke$<!ayck[TtB[d&|Q]h;8+j6 p%N K+;<Emy[Ak+aV%Zp');
define('SECURE_AUTH_KEY',  '2=beU+~z)ED;0S.C#R?VXd;NlFKb<j!~%%HzrXrM<nj5 ^WaJXxH73*/;1C+Z1LR');
define('LOGGED_IN_KEY',    'Bpl2#fzd>f31%1ftx+NMe0k0M(>9[ ,Bp{w=G&,4<*%~4}n/({Eq.}s&;r.TUvfn');
define('NONCE_KEY',        ']#e?Fdi&N}%n)=`1}Ozp.70&]pN2W;g+`2WvN42d!HhB8!aJR<`r1dQl+JM~hhML');
define('AUTH_SALT',        '@Ad.[fgDhkp>Z2Yk>rO|pDC-[Lr:V/)?-Cu6:AX@Qd+Q+#V>C>9h9T)?LzA&k2}P');
define('SECURE_AUTH_SALT', '8J+jQq6<iW1hL*m>AqYIuf[v31Od3fl,In[i|N_?8Hxp):tP6QY&n!+-NBrM%Sn/');
define('LOGGED_IN_SALT',   '1,4w8]C<lC,;;F!x,R>rm/_Q|Z6ogFc=G0k.X04%)i%UhJ;E~ J;UTuB/)EHJ`@m');
define('NONCE_SALT',       'rZnsY(3#?[hb40byYGQ:)XmxD5@/?%|{P.$ RY [R?DHr>?NT4pi)DV2pDT^TA< ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
